<?php include "header.php" ?>

<div class="container">
  <form action="date.php" method="get">
    Give a date: <br>
    <input type="text" name="day" size="4" placeholder="Day"> .
    <input type="text" name="month" size="4" placeholder="Month"> .
    <input type="text" name="year" size="6" placeholder="Year"><br>
    <input type="submit" name="send" value="Calculate">
  </form>

  <?php
    if (isset($_GET["send"])) {
      $day = $_GET["day"];
      $month = $_GET["month"];
      $year = $_GET["year"];

      if ((($day>0)&&($day<32))&&(($month>0)&&($month<13))&&(($year>0)&&($year<2200))) {
        print("<p>You gave the date ".$_GET["day"].".".$_GET["month"].".".$_GET["year"]."</p>");

        //todays date
        $today = date_create();
        print("<p>Todays date is ".date_format($today,"d.m.Y")."</p>");

        //given date
        $your_date = date_create($year."-".$month."-".$day);
        print("<p>".date_format($your_date,"d.m.Y")."</p>");

        $date_difference = date_diff($today,$your_date);
        $c_day = $date_difference->format("%R%a days, ");
        $c_hour = $date_difference->format("%R%h hours, ");
        $c_minute = $date_difference->format("%R%i minutes, ");
        $c_second = $date_difference->format("%R%s seconds");

        print("<p>Difference is " . $c_day . $c_hour . $c_minute . $c_second . ".</p>");

        //check if date is in the future or not
        if ($your_date < $today) {
          echo "Your date is in the past";
        }
        else {
          echo "Your date is in the future";
        }

      }
      else {
        print("<p>You did not give a proper date!</p>");
      }
    }
    else {
      print("<p>Fill the form!</p>");
    }
  ?>

</div>

</body>
</html>
