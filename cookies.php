<?php
  $cookie_name = "user";
  $cookie_value = $_SERVER['USER'];
  $cookie_expire = 86400 * 30; // 86400 = 1 day
  $date = new DateTime();

  setcookie($cookie_name, $cookie_value, time() + $cookie_expire, "/");

  include "header.php"
?>

<div class="container">
  <?php
  if(!isset($_COOKIE[$cookie_name])) {
     echo "Cookie named '" . $cookie_name . "' is not set!";
  }
  else {
     echo "<p>Cookie '" . $cookie_name . "' is set!</p>";
     echo "<p>Value is: " . $_COOKIE[$cookie_name] . "</p>";
     echo "<p>The cookie was set at: " . $date->getTimestamp() . "</p>";
  }
  ?>
</div>

</body>
</html>
