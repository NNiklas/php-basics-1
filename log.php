<?php include "header.php" ?>

<div class="container">
  <?php
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
    
    $ip = $_SERVER["REMOTE_ADDR"];
    $user = $_SERVER["REMOTE_USER"];
    $time = date("H:i:s j.n.Y");
    $myfile = fopen("logbook.log", "a+") or die("The file could not be opened!");
    $txt = "user = ".$user." | ip = ".$ip." | time = ".$time."\n";
    fwrite($myfile, $txt);
    fclose($myfile);
    print("<p>The following data was written in the log <br/>".$txt."</p>");
    print("<p><a href='logbook.log'>Logbook</a>.</p>");
  ?>
</div>
</body>
</html>
