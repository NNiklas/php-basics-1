<?php include "header.php" ?>
  <div class="container">

    <?php
      function test_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
      }
      
      $target_dir = "images/";
      $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

      // Check if image file is a actual image or fake image
      if(isset($_POST["submit"])) {
          $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
          if($check !== false) {
              echo "<p>The file is an image" . $check["mime"] . ".<br/>";
              $uploadOk = 1;
          } else {
              echo "<p>The file is not an image.<br/>";
              $uploadOk = 0;
          }
      }

      // Check if file already exists
      if (file_exists($target_file)) {
          echo "<p>Sorry, the file already exists.<br/>";
          $uploadOk = 0;
      }

      // Check file size
      if ($_FILES["fileToUpload"]["size"] > 2000000) {
          echo "Sorry, the file is too large.<br/>";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) {
          echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.<br/>";
          $uploadOk = 0;
      }

      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo "Sorry, your file cannot be uploaded.</p>";

      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
              echo "<p>The file ". basename( $_FILES["fileToUpload"]["name"]). "has been uploaded and is available <a href='images/'>here</a>.</p>";
          } else {
              echo "<p>Sorry, there is something wrong with your file.</p>";
          }
      }
      $catalog = "images/";
      $contents = scandir($catalog);
      print("<p>The catalog  now contains these files:<br/>");
      foreach ($content as $row) {
          if(!(($row==".")||($row==".."))) {print(" - ".$row." <br/>");}
      }
      print("</p>");
    ?>

  </div>
</body>
</html>
