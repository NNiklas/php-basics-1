<?php include "header.php" ?>

<div class="container">
  <?php
    session_start();
    session_destroy();
    echo '<p>You are now logged out!</p><a href="session.php"><button>Go back</button></a>';
  ?>
</div>
</body>
</html>
