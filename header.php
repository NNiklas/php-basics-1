<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/style.css">

  <title>PHP Basics</title>
</head>
<body>

  <nav>
    <div class="container">
      <h4>PHP Basics</h4>
      <ul>

        <li><a href="system.php">System</a></li>
        <li><a href="time.php">Time</a></li>
        <li><a href="date.php">Date</a></li>
        <li><a href="email.php">Email</a></li>
        <li><a href="cookies.php">Cookies</a></li>
        <li><a href="session.php">Session</a></li>
        <li><a href="files.php">Files</a></li>
        <li><a href="log.php">Log</a></li>
        <li><a href="guest.php">Guest</a></li>

      </ul>
    </div>
  </nav>
